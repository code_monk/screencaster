
import { receiverApplicationId, messageNamespace, replNamespace, Dom } from "./globals";

//import peerjs from 'https://cdn.skypack.dev/peerjs';
//import {typesChromecastCafSender as cast} from 'https://cdn.skypack.dev/@types/chromecast-caf-sender';

const $text = Dom.node("#text");
const $button = Dom.node("#send-text");

let rpc;
window.rpc = rpc;
let rp;
window.rp = rp;

let castSession = null;

const init = (isAvailable) => {
	if (isAvailable) {
		cast.framework.CastContext.getInstance().setOptions({
			receiverApplicationId,
			autoJoinPolicy: chrome.cast.AutoJoinPolicy.ORIGIN_SCOPED
		});
		rpc = new cast.framework.RemotePlayerController();
		rp = new cast.framework.RemotePlayer();
	}
};
window['__onGCastApiAvailable'] = init;

const sendText = () => {
	const txt = $text.value;
	castSession = cast.framework.CastContext.getInstance().getCurrentSession();
	if (castSession) {
		castSession.sendMessage(messageNamespace, {
			type: "message",
			text: txt
		});
	}
};
$button.addEventListener('click', sendText);

const sendCodeForEvaluation = (txt) => {
	castSession = cast.framework.CastContext.getInstance().getCurrentSession();
	if (castSession) {
		castSession.sendMessage(replNamespace, {
			type: "message",
			text: txt
		});
	}
};
Dom.node('#g').addEventListener('submit', ev => {
	ev.preventDefault();
	sendCodeForEvaluation(ev.target.code.value);
	return false;
});

const sendPing = () => {
	castSession = cast.framework.CastContext.getInstance().getCurrentSession();
	if (castSession) {
		castSession.sendMessage('urn:x-cast:com.google.cast.tp.heartbeat', {
			type: "PING"
		});
	}
};
window.setInterval(sendPing, 5000);