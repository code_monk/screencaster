import { messageNamespace, replNamespace, Dom } from "./globals";

const $msg = Dom.node('#message');
const $replOutput = Dom.node('#out');

const context = cast.framework.CastReceiverContext.getInstance();

//	simple Text
context.addCustomMessageListener(messageNamespace, (customEvent) => {
	if (customEvent.data.type == "message") {
		$msg.innerHTML = customEvent.data.text;
	}
});

//	REPL
context.addCustomMessageListener(replNamespace, (customEvent) => {
	if (customEvent.data.type == "message") {
		try {
			const r = eval(customEvent.data.text);
			$replOutput.innerText = JSON.stringify(r, null, "\t");
		} catch (e) {
			$replOutput.innerHTML = `<span class="error">${JSON.stringify(e, null, "\t")}</span>`;
		}
	}
});

//	keepalive
context.addCustomMessageListener('urn:x-cast:com.google.cast.tp.heartbeat', (customEvent) => {
	if (customEvent.data.type == "PING") {
		context.sendCustomMessage('urn:x-cast:com.google.cast.tp.heartbeat', undefined, { type: "PONG" });
	}
});

context.start();
