//const receiverApplicationId = chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID;
const receiverApplicationId = '956EBFB6';
const messageNamespace = 'urn:x-cast:ch.cimnine.cromecast-test.text';
const replNamespace = 'urn:x-cast:ca.fukt.cromecast-test.repl';

//	select and cache references to dom nodes
class Dom {
	static _map = new Map();
	static node(query: string) {
		const m = Dom._map;
		if (m.has(query)) {
			return m.get(query);
		} else {
			let el = document.querySelector(query);
			if (el !== undefined) {
				m.set(query, el);
			}
			return el;
		}
	}
	static nodes(query: string) {
		const m = Dom._map;
		if (m.has(query)) {
			return m.get(query);
		} else {
			let el = document.querySelectorAll(query);
			if (el !== undefined) {
				m.set(query, el);
			}
			return el;
		}
	}
}

export { receiverApplicationId, messageNamespace, replNamespace, Dom };